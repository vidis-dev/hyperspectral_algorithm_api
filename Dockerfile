FROM python:3.9-slim

RUN apt-get update
RUN apt-get install -y \
    'default-libmysqlclient-dev' \
    'build-essential'

RUN pip install vidis-algorithms-api
