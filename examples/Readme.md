# Examples

In this folder two examples are provided:

## dummy_example

Contains a simple example returns empty picture using docker image based on `banayaki/vidis-algorithms`

## neural_network

Contains an advanced example returns neural network's prediction using custom docker image