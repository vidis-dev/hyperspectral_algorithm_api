# Neural network deployment in Vidis
In order to deploy more complex algorithms in Vidis you may need to create your Docker image from scratch.
This example shows how to achive that.

As in dummy example, firstly you need to implement `run` and `get_type_name` methods.

The implementation may look like this
```python
class Algorithm(Task):
    def run(self, hyperspecter: np.ndarray, **kwargs) -> np.ndarray:
        shape = hyperspecter.shape[1:]
        result = predict(hyperspecter)
        seg_map = make_seg_map(result)
        # NN always returns even shape
        seg_map = cv2.resize(seg_map, shape)
        return seg_map

    def get_type_name(self) -> str:
        return 'oursnet'
```

# Dockerfile
Pay the most attention to new dockerfile. 
The base image not is a custom image. In our case `bitnami/pytorch:1.13.0`.
Try to use lightweight images as much as possible.

Since `vidis_algorithms_api` has it's own dependencies you must satisfy them in your image too.

Firstly you need to install Maria Database C Connector with version higher than 3.1.5 (?). To achive that you may reuse
the code provided in this example

```dockerfile
RUN apt-get update
RUN apt-get install -y \
    'curl' \
    'default-libmysqlclient-dev' \
    'build-essential'

RUN curl -sS  https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | bash

RUN apt-get install -y libmariadb3 libmariadb-dev
```

Secondly, you need to install required python dependencies. To achive that, please, copy dependencies of the `vidis_algorithms_api` in
your `requirements.txt` file. (They're listed in `setup.py` file)
At the moment of writing current readme the following packages are have to be installed:

```
pika==1.3.1
mariadb==1.0.7
mysqlclient==2.0.3
pydantic==1.10.2
loguru==0.6.0
sqlalchemy==1.4.42
numpy==1.23.4
```