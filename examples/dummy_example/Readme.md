# Dummy example

Comprised of two files - python script implemented dummy hyperspecter processing and
dockerfile from which the image is built

Following steps have to be completed in order to use `vidis_algorithms_api`

1. You should implement `run` and `get_type_name` methods inheriting `Task` class from `vidis_algorithms_api` package
In `run` method your algorithm should be called. This method provides with hyperspecteral data in the form of numpy.ndarray which
has shape of [channels, height, width] and dictionary which contains parameters specified on the frontend side.
`run` must return a matrix (gray-scale image) with same spatial shape as hyperspecter
2. Script should initialize class which you implemented and invoke `serve()` method

This particular example for any given hyperspecter returns matrix of zeros of the same shape that hyperspecter has

# Dockerfile

You may use special image `banayaki/vidis-algorithms` to create your own.
It based on vanila python 3.9 with a few packages required by `vidis_algorithms_api`

# Important

Beer in mind that `get_type_name()` should returns string which satisfies the following regular expression:

`^([a-z]+|\d+|[_]+)$`

*(In other words in should be writtent in lower case using only letters, numberse and underscores (_))*